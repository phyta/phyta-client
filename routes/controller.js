const express = require('express')
const router = express.Router()
const passport = require('passport')
const service = require('./../service/mongoService.js')
const request = require('request-promise')
const fs = require('fs')

const api = 'http://localhost:5000'

router.get('/', (req, res, next) => res.redirect('/home'))

router.get('/home', (req, res, next) => {
    res.render('home', { user: req.user })
})

router.get('/process', (req, res, next) => {
    request({uri: api + '/configFile', json: true})
    .then(result =>{
        res.render('process', { user: req.user, metrics: result.metrics, types: result.trees})
    }) 
    .catch(err => next(err))    
})

router.post('/process', (req, res, next) => {
    const body = { 
        trees: req.body.trees.join('$'), 
        treeTypes: req.body.treeTypes.join('$'), 
        metrics: req.body.metrics.join('$'), 
        parser: 'newick'
    }
    if(req.body.email != "")
        body.email = req.body.email 

    const options = {
        method: 'POST',
        uri: api + '/process',
        form: body
    }
    request(options)
    .then(result => {
        res.send(result)
    }) 
    .catch(err => {
        res.send(JSON.parse(err.error))
    }) 
})

router.get('/searchResults', (req, res, next) => {
    res.render('searchResults', { user: req.user })
})

router.get('/export/:id', (req, res, next) => {
    const id = req.params.id
    const options = {
        method: 'GET',
        uri: api + '/checkResults/' + id,
        json: true
    }
    request(options)
    .then(results => {
        res.send(JSON.stringify(results))
    })
    .catch(err => next(err))
})

router.get('/checkResults', (req, res, next) => {
    const id = req.query.id.replace(/\s/g, "") 

    const options = {
        method: 'GET',
        uri: api + '/checkResults/' + id,
        json: true
    }

    request(options)
    .then(results => {

        if(results.error != undefined){
            res.render('error', {results, user: req.user})
            return
        }

        const formats = {}
        results.trees.forEach(t => {
            formats[t.id] = { 'format': t.format, 'type': t.type }
        })
        results.formats = JSON.stringify(formats)

        results.results.forEach(arr => {
            arr.forEach(r => {
                r.results.forEach(res => {
                    res.result.diffsT1 = JSON.stringify(res.result.diffsT1.sort((d1, d2) => d1.id - d2.id))
                    res.result.diffsT2 = JSON.stringify(res.result.diffsT2.sort((d1, d2) => d1.id - d2.id))
                })
            })
        })

        res.render('results', { results , export: JSON.stringify(results), user: req.user})
    })
    .catch(err => next(err))
    
})

router.get('/myJobs', isLoggedIn, (req, res, next) => {
    service.getDocument(req.user.email)
    .then(results =>{
        res.render('myJobs', { user: req.user, jobs: results.jobs })
    })
})

router.get('/login', (req, res, next) => {
    res.redirect('/auth/google')
})

router.get('/auth/google',
    passport.authenticate('google', { 
        scope: [ 'https://www.googleapis.com/auth/plus.login',
          'https://www.googleapis.com/auth/plus.profile.emails.read' ]
    })
)

router.get('/auth/google_oauth2/callback',
    passport.authenticate('google', { failureRedirect: '/home' }),
    function(req, res) {
        res.redirect('/')
    }
)

// route for logging out
router.get('/logout', (req, res) => {
    req.logout()
    res.redirect('/')
})

router.post('/addJobId', (req, res, next) => {
    req.user.jobs.push(req.body.id)
    service.addNewJob(req.body.email, req.body.id)
})

router.delete('/deletejob/:jobid',isLoggedIn,(req,res,next)=>{
    const jobid = req.params.jobid
    service.removeJob(req.user.email, jobid)
    
    let options = {
        method: 'DELETE',
        uri: 'http://localhost:5000/deletejob/' + jobid
    }
    
    request(options)
    .then(results =>{
        res.send(results)
    })
    .catch(err =>{
        next(err)
    })

})

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next()
    // if they aren't redirect them to the home page
    res.redirect('/')
}

module.exports = router