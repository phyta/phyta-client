'use strict'
const mongoDb = require('mongodb')
const connection = require('mongoose').connection

let User = connection.model('User',{
    oauthID: Number,
    name: String,
    email: String,
    jobs: []
})

module.exports = User