const polyfill = require('phylocanvas/polyfill')

const phylocanvas = require('phylocanvas')

console.log(phylocanvas)

const contextMenu = require('phylocanvas-plugin-context-menu')
const history = require('phylocanvas-plugin-history')
const scalebar = require('phylocanvas-plugin-scalebar')

phylocanvas.plugin(scalebar)
phylocanvas.plugin(history)
phylocanvas.plugin(contextMenu)

module.exports = phylocanvas