function ajaxRequest(meth, path, data) {
    return fetch(path, {
        method: meth,
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data),
        credentials: 'same-origin'
    })
    .then(resp => {
        if(resp.status != 200)
            throw new Error(resp.statusText)
        return resp.text()
    })
}

function exportRes(id){
    ajaxRequest('GET', '/export/' + id)
    .then(results => {
        const blob = new Blob([decodeURIComponent(results)], {type: 'text/json'})
        let a = document.createElement('a')
        a.href = URL.createObjectURL(blob)
        a.setAttribute('download', 'results.json')
        a.innerText = 'Export'
        a.className = 'expText'

        let btn = document.getElementById('btn')
        btn.appendChild(a)
    })
}

function addTree(){
    let tree = document.getElementById('treeData').value
    tree = tree.replace(/\s/g, '')
    const typeData = document.getElementById('typeData')
    let type = typeData.options[typeData.selectedIndex].value
    type = type.replace(/\s/g, '')
    const table = document.getElementById('trees')
    if(tree == ''){
        alertify.error('Please insert a tree')
    }
    else if(type == ''){
        alertify.error('Please insert a type')
    }
    else{
        const row = table.insertRow(0)
        const cell2 = row.insertCell(0)
        cell2.innerHTML = type
        cell2.className = 'type'
        const cell1 = row.insertCell(0)
        cell1.innerHTML = tree
        cell1.className = 'tree'
        alertify.success('Tree added')
        document.getElementById('treeData').value = ''
    }
}

function deleteJob(jobId){
    let joblist = document.getElementById('jobList')
    let job = document.getElementById(jobId)
    ajaxRequest('DELETE','/deletejob/' + jobId, undefined)
    .then(result =>{
        joblist.removeChild(job)
        alertify.success('Job removed successfully!')
    })
    .catch(err =>{
        alertify.error('Cannot delete job')
    })
}

function cleanTrees(){
    let table = document.getElementById('trees')
    table.innerHTML = ''
    alertify.success('Successfully deleted all data inputed')
    return table
}

function process(email){
    const table = document.getElementById('mytable')
    const trees = Array.from(table.getElementsByClassName('tree')).map(t => t.innerText)
    const treeTypes = Array.from(table.getElementsByClassName('type')).map(t => t.innerText)
    const metrics = Array.from(document.getElementById('metrics')).filter(o => o.selected).map(t => t.value)

    ajaxRequest('POST', '/process', { trees, treeTypes, metrics, parser: 'newick', email: email })
    .then(res => {
        res = JSON.parse(res)
        if(res.error != undefined){
            alertify.error(res.error)
        }
        else{
            if(email) ajaxRequest('POST','/addJobId', {email,'id': res.id})
            $('#jobId').val(res.id)
            $('#hrefId').attr('href', (i, str) => str + res.id)
            $('#modal').modal('show')
        }
    })
    .catch(err => alertify.error(err.message))
}

function viewComparison(formats, diffsT1, diffsT2, tree1Id, tree2Id){
    document.getElementById('header').innerText = 'Tree ' + tree1Id + ' vs Tree ' + tree2Id
    document.getElementById('segment1').innerHTML = ''
    document.getElementById('segment2').innerHTML = ''

    diffsT1 = JSON.parse(diffsT1), diffsT2 = JSON.parse(diffsT2), formats = JSON.parse(formats)
    
    const t1 = drawTree('segment1', treeConfig, config[formats[tree1Id].type], diffsT1)
    const t2 = drawTree('segment2', treeConfig, config[formats[tree2Id].type], diffsT2)
    
    if(formats[tree1Id].type == 'forest') drawForest(t1, formats[tree1Id].format, 'segment1', 'd1')
    else t1.load(formats[tree1Id].format)

    if(formats[tree2Id].type == 'forest') drawForest(t2, formats[tree2Id].format, 'segment2', 'd2')
    else t2.load(formats[tree2Id].format)
}

function drawForest(tree, format, divId, dropdownId){
    const div = document.querySelector('#' + divId)
    frag = document.createDocumentFragment()
    select = document.createElement('select')
    select.className = 'ui search dropdown'
    select.id = dropdownId
    format.split(';').forEach((t, idx) => {
        if(t != '') select.options.add( new Option('Subtree ' + idx, t) )
    })
    select.addEventListener('change', () => {
        const elem = document.getElementById(dropdownId)
        let subtree = elem.options[elem.selectedIndex].value
        tree.load(subtree)
    })
    frag.appendChild(select)
    div.appendChild(frag)
    const f = format.split(';')[0]
    tree.load(f)
}

function drawTree(div, config, type, diffs){
    const t = Phylocanvas.createTree(div, config)
    t.setTreeType(type)
    t.setNodeSize(5)
    t.on('beforeFirstDraw', () => {
        colourDiffs(t, type, diffs)
        collapseBigTrees(t)
    })
    return t
}

function colourDiffs(tree, type, diffs){
    if(diffs.length == 0) return
    if(type == 'forest'){
        
        let index = 0
        visit(tree.root, b => {
            let colour = getColour(diffs[index].dif)
            b.colour = colour
            b.internalLabelStyle = {textSize: 5, colour: colour}
            index++
        })
    }
    let index = 0
    visit(tree.root, b => {
        let colour = getColour(diffs[index].dif)
        b.colour = colour
        b.internalLabelStyle = {textSize: 5, colour: colour}
        index++
    })
}

/**
 * Post order visit in tree
 * 
 * @param {any} elem 
 * @param {any} func 
 */
function visit(elem, func){
    func(elem)
    elem.children.forEach(e => {
        this.visit(e, func)
    })
}

function getColour(dif){
    if(dif == 0) return '#4575b4'
    else if(dif == -1) return '#d73027'
    else return scale(dif).hex()
}

function collapseBigTrees(tree){
    tree.root.children.forEach(c => tree.setInitialCollapsedBranches(c))
    tree.root.expand()
}

function tableRow(row) {
    const tr = document.createElement('tr')
    tr.innerHTML = row
    return tr
}

function stringToHtml(str) {
    const div = document.createElement('div')
    div.innerHTML = str
    return div.firstChild
}

config = {
    'rooted': 'rectangular',
    'unrooted': 'radial',
    'forest': 'radial'
}

scale = chroma
    .scale(['#74add1', '#abd9e9', '#e0f3f8', '#fee090', '#fdae61', '#f46d43'])
    .classes(6)

treeConfig = { 
    alignLabels: true, 
    lineWidth: 2,
    selectedColour: 'black',
    highlightColour: 'black',
    branchColour: '#4575b4',
    showInternalNodeLabels: true,
    showBranchLengthLabels: true,
    textSize: 3,
    defaultCollapsed: { min: 20, max: 99999 },
    internalLabelStyle: { textSize: 5, colour: '#4575b4' }
}