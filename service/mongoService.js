'use strict'
const mongoDb = require('mongodb')
const connection = require('mongoose').connection
const collection = 'users'

function addNewJob(email, jobId){
    return connection
        .collection(collection)
        .updateOne(
            {'email': email},
            { $push: {jobs: jobId } }
        )
}

function removeJob(email, jobId){
    return connection
    .collection(collection)
    .updateOne(
        {'email': email},
        {$pull:{jobs:{$in: [jobId]}}},
        {multi: true}
    )
}

function getDocument(email){
    return connection
        .collection(collection)
        .findOne({'email': email})
}

module.exports = {
    addNewJob,
    removeJob,
    getDocument
}