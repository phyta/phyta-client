let express = require('express')
let path = require('path')
let favicon = require('serve-favicon')
let logger = require('morgan')
let cookieParser = require('cookie-parser')
let bodyParser = require('body-parser')
let passport = require('passport')
let GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
let config = require('./oauth')
let session = require('express-session')
let User = require('./model/users.js')
const mongoose = require('mongoose')

let controller = require('./routes/controller')

let app = express()

const url = 'mongodb://localhost:27017/phyta-client'
const options = {server: {socketOptions: {keepAlive: 1}}}
mongoose.connect(url, options)

//To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.
passport.serializeUser(function(user, done) {
  done(null, user._id)
})

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user){
      if(!err) done(null, user)
      else done(err, null)
  })
})

passport.use(new GoogleStrategy({
  clientID: config.google.clientID,
  clientSecret: config.google.clientSecret,
  callbackURL: config.google.callbackURL
},
function (accessToken, refreshToken, profile, done) {
  process.nextTick(function () {
      User.findOne({oauthID: profile.id}, function (err, user) {
          if (err) {
              done(err)
          }
          if (!err && user !== null) {
              done(null, user);
          }
          else {
              user = new User({
                  oauthID: profile.id,
                  name: profile.displayName,
                  email: profile.emails[0].value,
                  jobs: []
              })
              user.save(function (err) {
                  if (err) {
                      console.log(err)
                      throw err
                  }else {
                      done(null, user)
                  }
              })
          }
      })
  })
}
))

// view engine setup
const hbs = require('hbs')
//path to serve the hbs views
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs');
//register all partials in folder partials
hbs.registerPartials(__dirname + '/views/partials')

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(logger('dev'))
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
app.use(cookieParser())
app.use(express.static(__dirname))
app.use(session({secret: 's3Cur3',name: 'sessionId'})) //http://expressjs.com/en/advanced/best-practice-security.html
app.use(passport.initialize())
app.use(passport.session())

app.use('/', controller)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error',{err,user: req.user})
})

module.exports = app